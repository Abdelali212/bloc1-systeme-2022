# Mission 1 : Découverte d'une première Solution technique d'accès (STA)
#### [Zoubeïda ABDELMOULA](mailto:zoubeida.abdelmoula@gmail.com), ESIEE-IT
#### Cours Bloc1 Système (BTS - SIO) - 28/09/2022



## Objectif


L'objectif de cette activité est d'apprendre décrire une solution technique d'accès (STA) et repérer les mots techniques à utiliser.



## Description du matériel personnel

Créer un nouveau document sur votre dépôt GitLab, pour permettre de mieux connaître votre solution technique d'accès (`B1_Systeme/STA_personnelle.md`). Mettre en forme le contenu avec Markdown. 

- la marque
- les composants essentiels pour le bon fonctionnement de la STA et leurs caractéristiques
- les connecteurs disponibles, pour connecter quel type de périphérique. 
- le système d'exploitation et ses caractéristiques
- logiciels installés - 10 plus utilisés (version, type de licence)
- possibilités d'évolution matériel ou logiciel
- *et toute autre information logicielle ou matérielle importante !!*

Compléter un **aide mémoire personnel** avec tous les marqueurs markdown découverts ou utilisés et enregister-le sous le nom `B1_Systeme/aide_markdown.md`


## A la fin de l'activité...

On devra trouver sur votre dépôt 2 nouveaux documents (+ les images) et un document modifié : 

- votre premier aide-mémoire : `B1_Systeme/aide_markdown.md`
- votre description de matériel personnel : `B1_Systeme/STA_personnelle.md`
- vos images dans le repertoire : `B1_Systeme/img`

