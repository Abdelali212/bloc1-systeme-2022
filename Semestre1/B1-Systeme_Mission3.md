## Mission 3 : Utilisation du terminal (MS-DOS / Terminal linux)
#### [Zoubeïda ABDELMOULA](mailto:zoubeida.abdelmoula@gmail.com), ESIEE-IT
#### Cours Bloc1 Système (BTS - SIO) - 09/11/2022





## Objectif


L'objectif de cette activité est de proposer une **anti-sèche** des commandes Windows et Linux souvent utilisées dans le terminal d'une solution technique d'accès.  

Voici quelques liens vers des applications utiles : 

- émuler un terminal ubuntu dans windows : <https://www.microsoft.com/fr-fr/p/ubuntu-1804-lts/9n9tngvndl3q?rtc=1&activetab=pivot:overviewtab>
- Emuler un terminal MS-DOS dans MacOS : <http://www.ordinateur.cc/syst%C3%A8mes/Mac-OS/209654.html>
- Convertisseur de Markdown en PDF : <https://pandoc.org/getting-started.html> 

## Contraintes

L'anti-sèche sera bien identifiée (Titre, Nom, date) et présentée sous forme de tableau sur **2 pages**.

Les livrables markdown et pdf (via pandoc) sont attendus sur le dépôt GitLab (2 pages max attendue).


## Expérimentations via des exercices 

Plusieurs exercices seront proposées pour vous permettre d'explorer les différentes commandes souvent utilisées.

Voici des éléments importants à détailler : 

- Trouver pour chaque commande Windows, son équivalent Linux ou inversement (c'est pas toujours possible)
	- un exemple à l'appuie avec des chemins relatifs et/ou absolus peut être utile 
- Expliquer pour chaque commande explorée les éléments obligatoires (<...>) ou optionnels ([...])
- Rappeler pour chaque système d'exploitation le chemin du dossier personnel d'un utilisateur
 


## A la fin de l'activité...

On devra trouver sur votre dépôt 4 documents : 

- Les Exercices commentés sur la ligne de commande windows : `B1_Systeme/ExercicesCMD-W10.md`
- Les Exercices commentés sur la ligne de commande linux : `B1_Systeme/ExercicesCMD-Linux.md`
- votre anti-sèche en markdown : `B1_Systeme/anti-seche-cmd.md`
- votre anti-sèche en pdf (2 pages) : `B1_Systeme/anti-seche-cmd.pdf`

