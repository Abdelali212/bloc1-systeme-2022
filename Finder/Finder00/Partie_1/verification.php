<?php session_start();
if(
    isset($_GET['email'])
    && isset($_GET['password'])
){
    
    $dsn='mysql:dbname=finder;host=127.0.0.1';
    $user='root';
    $password='';
    try{
        $dbh=new PDO($dsn,$user,$password);
    }catch(PDOException $e){
        echo'Connexion échouée:'.$e->getMessage();
    }
    $sql = "SELECT count(*) FROM user WHERE email=:email
   AND password=PASSWORD(:password)";
    
    $resultats = $dbh->prepare($sql);
    $email = $_GET['email'];
    $password = $_GET['password'];
    $resultats->bindParam(":email", $email);
    $resultats->bindParam(":password", $password);
    $resultats->execute();
    $number_of_rows = $resultats->fetchColumn();

    if($number_of_rows == 1){
        echo "ok";
    }
    else{
        $_SESSION['erreur']=true;
        header('Location: http://localhost/connexion.php');
    }
}else{
    $_SESSION['erreur']=true;
    header('Location: http://localhost/connexion.php');
}
