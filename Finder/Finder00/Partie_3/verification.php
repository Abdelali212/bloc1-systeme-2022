<!doctype html>
<html lang="fr">
<head>
    <link type="text/css" rel="stylesheet" href="https://unpkg.com/bootstrap/dist/css/bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="https://unpkg.com/bootstrap-vue@latest/dist/bootstrap-vue.css" />
    <script src="https://unpkg.com/vue"></script>
    <script src="https://unpkg.com/bootstrap-vue@latest/dist/bootstrap-vue.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
</head>
<body>
    <div id="app">
        <b-container>
            <input v-model="email" placeholder="Email" />
            <input v-model="password" type="password" placeholder="Password" />
            <b-btn variant="primary" v-on:click="ajax">Valider</b-btn>
            <div v-if="error" class="alert alert-danger">{{ error }}</div>
            <div v-if="message" class="alert alert-success">{{ message }}</div>
        </b-container>
    </div>

    <script>
        var app = new Vue({
            el: '#app',
            data() {
                return {
                    email: '',
                    password: '',
                    error: '',
                    message: ''
                }
            },
            methods: {
                ajax: function () {
                    axios.post('http://localhost/finder1/verification2.php', {
                        email: this.email,
                        password: this.password
                    })
                    .then(response => {
                        if (response.status === 200) {
                            this.error = '';
                            this.message = 'Connexion réussie';
                        } else {
                            this.error = 'Identifiants de connexion incorrects';
                            this.message = '';
                        }
                    })
                    .catch(error => {
                        this.error = 'Une erreur s\'est produite lors de la demande AJAX';
                        this.message = '';
                    });
                }
            }
        });
    </script>
</body>
</html>
