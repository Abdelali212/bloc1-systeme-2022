#   Créer une clé SSH et l'utiliser

Auteur :
    MEYER Alexis

Date : 
    01/11/22

---

## Sommaire

| Numéro | Rubrique                                  |
| ------ | ----------------------------------------- |
|   1    | Exigences                                 |
|   2    | Qu'est-ce qu'une clé SSH ?                |
|   3    | Générer une clé SSH                       |
|   4    | Utiliser une clé SSH                      |
|   4.1  | GitLab                                    |
|   4.2  | GitHub                                    |
|   5    | Cloner un repository avec une adresse SSH |
|   6    | Conclusion                                |

## 1. Exigences :

1.  Avoir Git installé.

2.  Avoir son ordinateur à jour.

## 2. Qu'est-ce qu'une clé SSH ?

D'après [Gnome.org](https://help.gnome.org/users/seahorse/stable/about-ssh.html.fr#:~:text=Une%20clé%20Secure%20Shell%2C%20le,sur%20un%20réseau%20non-sécurisé.) :

>   "Une clé Secure Shell, le plus souvent appelée clé SSH, vous permet de créer une connexion sécurisée sur un réseau non-sécurisé."

Elle est composée de deux clés nommées respectivement :

- Clé publique :
    
    - Permet de chiffrer (rendre illisible) des informations.

- Clé privée.

    - Permet de déchiffrer (rendre lisible) des informations chiffrées avec la clé publique associée.

---

## 3. Générer une clé SSH

Lancer GIT **BASH**.

Oui, nous allons devoir utiliser des lignes de commandes, mais ne vous inquiétez pas, ce n'est pas long.

```
cd ~
```

Cette première commande positionnera le dossier actif à la racine votre dossier utilisateur.

```
ssh-keygen -b 4096
```

Analysons cette commande :

`ssh-keygen` est le nom de la commande que nous exécutons et qui va créer notre clé SSH pour nous.

`-b` est un argument qui signifie que nous voulons une taille spécifique pour notre clé.

`4096` le nombre de bits que doit faire notre clé.

Il existe d'autres options pour cette commande, cependant nous allons nous contenter de celle-ci pour ce tutoriel. [(Pour les autres options)](https://linux.die.net/man/1/ssh-keygen)

```
$ ssh-keygen -b 4096
Generating public/private rsa key pair.
Enter file in which to save the key (/c/Users/Alexis/.ssh/id_rsa):
```

Il vous est demandé dans quel fichier souhaitez-vous enregistrer votre clé SSH.
En gros quel sera le chemin + le nom du fichier dans lequel votre clé sera stocké.

Dans mon cas je souhaite que ma clé soit dans le dossier `/c/Users/Alexis/.ssh/`

et dans le fichier `testKey`

Je mettrai donc : `/c/Users/Alexis/.ssh/testKey`

**Attention si vous avez déjà une ou plusieurs clé(s) SSH veillez à ne pas donner le même nom de fichier, sinon elles seront écrasées et définitivement perdu.**

*Une confirmation vous sera demandée si vous vous apprêtez à écraser une clé existante.*

    Enter passphrase (empty for no passphrase):

Il est **très** recommandé de protéger votre clé SSH par un mot de passe pour éviter qu'un se fasse passer pour vous.

Ce mot de passe vous sera demandé à **CHAQUE** utilisation de votre clé.

**Notez-le.**


```
Your identification has been saved in /c/Users/Alexis/.ssh/testKey
Your public key has been saved in /c/Users/Alexis/.ssh/testKey.pub
The key fingerprint is:
SHA256:6vfOtsLTFYc9TsnYmUTmVvB+HTAbEva4AbsdpEbhusA xiiros@PC-fixe-Alexis
The key's randomart image is:
+---[RSA 4096]----+
|         +.=.=+..|
|        o * +o*o |
|         = + Xo=.|
|    .   o o B.@.o|
|     E .S. o = .+|
|      ...   . . .|
|      .o . .     |
|     .  =.o      |
|      .. *=.     |
+----[SHA256]-----+
```

Si un message de ce type apparait, c'est que tous s'est bien passé et votre clé SSH est maintenant créée.

---

## 4. Utiliser une clé SSH

Avant tout,

- Copiez votre clé SSH **PUBLIQUE** dans votre presse-papier.

    - Sur windows allez dans `C:\Users\[Votre nom de session]\.ssh\`
    - Sur Ubuntu allez dans `/home/[Votre nom d'utilisateur]/.ssh/`

    Ouvrez le fichier : `[nom de votre clé].pub` avec un éditeur de texte et copiez en le contenu.

### 4.1. GITLAB

-   Rendez-vous sur [gitlab.com](https://gitlab.com/)

-   Cliquez sur votre image de profil en haut à droit de votre fenètre.

-   Cliquez sur `Preferences`.

-   Cliquez sur l'onglet `SSH Keys` dans le panneau à gauche.

-   Collez votre clé dans le champ `Key`, donnez lui un nom et je vous conseille, pour un usage personnel de cours, de ne pas mettre de date d'expiration de la clé.

-   Cliquez sur `Add key`.

-   Allez sur le projet de votre choix.

-   Cliquez sur le bouton `Clone`.

-   Copiez l'adresse dans le champ `Clone with SSH`.

---

### 4.2. GITHUB

-   Rendez-vous sur [github.com](https://github.com/)

-   Cliquez sur votre image de profil en haut à droit de votre fenêtre.

-   Cliquez sur `Settings`.

-   Cliquez sur l'onglet `SSH and GPG Keys` dans le panneau à gauche.

-   Cliquez sur le bouton `New SSH key`

-   Collez votre clé dans le champ `Key`, donnez lui un nom et laissez le champ `Key type` sur `Authentification Key`.

-   Cliquez sur `Add SSH key`.

-   Allez sur le projet de votre choix.

-   Cliquez sur le bouton `Code`.

-   Allez à l'onglet `SSH`.

-   Copiez l'adresse.

---

## 5. Cloner un repository avec une adresse SSH

Dans votre terminal git bash pour cloner un repository il vous suffit de faire la commande : 

    git clone [adresse SSH]

---

## 6. Conclusion

Vous savez maintenant créer et utiliser une clé SSH !

Merci d'avoir suivi ce tutoriel, j'espère qu'il vous a été utile.